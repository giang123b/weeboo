package com.river.weeboo

import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.NavigationUI.onNavDestinationSelected
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpNavigation()
    }

    fun setUpNavigation() {
        val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.newFeedsFragment -> {
                    textView_tittle_homeScreen.text = getText(R.string.newfeeds)
                    onNavDestinationSelected(item, Navigation.findNavController(this, R.id.fragNavHost))
                }
                R.id.comicFragment -> {
                    textView_tittle_homeScreen.text = getText(R.string.truyen)
                    onNavDestinationSelected(item, Navigation.findNavController(this, R.id.fragNavHost))
                }
                R.id.novelFragment -> {
                    textView_tittle_homeScreen.text = getText(R.string.tieu_thuyet)
                    onNavDestinationSelected(item, Navigation.findNavController(this, R.id.fragNavHost))
                }
                R.id.bookShelvesFragment -> {
                    textView_tittle_homeScreen.text = getText(R.string.gia_sach)
                    onNavDestinationSelected(item, Navigation.findNavController(this, R.id.fragNavHost))
                }
            }
            false
        }
        bottom_nav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

    }
}