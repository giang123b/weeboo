package com.river.weeboo.newfeeds.domain.remote

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object PostClient {
    val getClient: ApiInterface
        get() {

            val client = OkHttpClient
                .Builder()
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl("https://giang123b.github.io/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)

        }

}