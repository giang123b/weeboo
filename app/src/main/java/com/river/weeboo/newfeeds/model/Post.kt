package com.river.weeboo.newfeeds.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Post(
    @SerializedName("id")
    @Expose
    val id: Int,

    @SerializedName("avatar")
    @Expose
    val avatar: String,

    @SerializedName("name")
    @Expose
    val name: String,

    @SerializedName("author")
    @Expose
    val author: String,

    @SerializedName("time")
    @Expose
    val time: String,

    @SerializedName("description")
    @Expose
    val description: String,

    @SerializedName("image")
    @Expose
    val image: String,

    @SerializedName("like")
    @Expose
    val like: Int,

    @SerializedName("comment")
    @Expose
    val comment: Int,

    @SerializedName("share")
    @Expose
    val share: Int,
) {
}