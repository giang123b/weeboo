package com.river.weeboo.newfeeds.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.river.weeboo.R
import com.river.weeboo.newfeeds.domain.Repository
import com.river.weeboo.newfeeds.model.Post
import com.river.weeboo.newfeeds.view_model.NewfeedsViewModel
import kotlinx.android.synthetic.main.fragment_new_feeds.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class NewFeedsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_new_feeds, container, false)
        val recyclerView_listPost_newFeedsScreen : RecyclerView = view.findViewById(R.id.recyclerView_listPost_newFeedsScreen)

//        val newfeedsViewModel: NewfeedsViewModel = ViewModelProviders.of(this).get(NewfeedsViewModel::class.java)

        val aListGroupPostAdapter = NewfeedsAdapter()
        recyclerView_listPost_newFeedsScreen.adapter = aListGroupPostAdapter

        recyclerView_listPost_newFeedsScreen.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

//        aListGroupPostAdapter.submitList(it)

        val repository = Repository()
        GlobalScope.launch (Dispatchers.Main){
            repository.getPost().observe(viewLifecycleOwner, {
                aListGroupPostAdapter.submitList(it)
            })
        }

        return view
    }

}