package com.river.weeboo.newfeeds.view_model

import androidx.lifecycle.ViewModel
import com.river.weeboo.newfeeds.domain.Repository
import com.river.weeboo.newfeeds.model.ChooseImageOrVideo
import com.river.weeboo.newfeeds.model.CreateNewPost
import com.river.weeboo.newfeeds.model.Post

class NewfeedsViewModel(private val repository: Repository) : ViewModel() {

//    fun getPost()= repository.getPost()

    fun loadAllData(): ArrayList<Any> {
        val mListNewfeeds = ArrayList<Any>()

        mListNewfeeds.add(CreateNewPost(
            "https://photographer.com.vn/wp-content/uploads/2020/08/1597480178_692_Loat-anh-Girl-Xinh-Cap-3-nu-sinh-tuoi-xi-teen.jpg"))

        mListNewfeeds.add(
            ChooseImageOrVideo()
        )

        mListNewfeeds.add(
            Post(
                1,
                "https://i.pinimg.com/236x/3f/c6/07/3fc607bd10827a23185a60253d0e4873.jpg",
                "Yêu xa",
                "1000",
                "10 phút",
                "The diagram below shows the stages and equipment used in the cement-making process, and how \n" +
                        "cement is used to produce concrete for building purposes.",
                "https://photographer.com.vn/wp-content/uploads/2020/08/1597480178_692_Loat-anh-Girl-Xinh-Cap-3-nu-sinh-tuoi-xi-teen.jpg",
                100, 100, 100
            )
        )

        mListNewfeeds.add(
            Post(
                2,
                "https://i.pinimg.com/236x/3f/c6/07/3fc607bd10827a23185a60253d0e4873.jpg",
                "Yêu xa",
                "1000",
                "10 phút",
                "The diagram below shows the stages and equipment used in the cement-making process, and how \n" +
                        "cement is used to produce concrete for building purposes.",
                "https://1.bp.blogspot.com/-iClsU_7cDrU/XcfYx2KesGI/AAAAAAAATcY/A428ov61qeQAE7ySUhak1KGOWXsOdRTFQCLcBGAsYHQ/s1600/Wap102Com-68-Anh-gai-xinh-mac-ao-dai%2B%25282%2529.jpg",
                100, 100, 100
            )
        )

        mListNewfeeds.add(
            Post(
                3,
                "https://photographer.com.vn/wp-content/uploads/2020/08/1597480178_692_Loat-anh-Girl-Xinh-Cap-3-nu-sinh-tuoi-xi-teen.jpg",
                "Yêu xa",
                "1000",
                "10 phút",
                "The diagram below shows the stages and equipment used in the cement-making process, and how \n" +
                        "cement is used to produce concrete for building purposes.",
                "https://photographer.com.vn/wp-content/uploads/2020/08/1597480178_692_Loat-anh-Girl-Xinh-Cap-3-nu-sinh-tuoi-xi-teen.jpg",
                100, 100, 100
            )
        )
        mListNewfeeds.add(
            Post(
                4,
                "https://imgix.bustle.com/uploads/image/2018/3/28/17a46e1b-05c4-425a-9c98-8802a5ddac71-girlshylaughingsweetjeanjacket.jpg?w=1020&h=574&fit=crop&crop=faces&auto=format%2Ccompress&cs=srgb&q=70",
                "Yêu xa",
                "1000",
                "10 phút",
                "The diagram below shows the stages and equipment used in the cement-making process, and how \n" +
                        "cement is used to produce concrete for building purposes.",
                "https://imgix.bustle.com/uploads/image/2018/3/28/17a46e1b-05c4-425a-9c98-8802a5ddac71-girlshylaughingsweetjeanjacket.jpg?w=1020&h=574&fit=crop&crop=faces&auto=format%2Ccompress&cs=srgb&q=70",
                100, 100, 100
            )
        )

        for (i in 1..10) {
            mListNewfeeds.add(
                Post(
                    10 + i,
                    "https://imgix.bustle.com/uploads/image/2018/3/28/17a46e1b-05c4-425a-9c98-8802a5ddac71-girlshylaughingsweetjeanjacket.jpg?w=1020&h=574&fit=crop&crop=faces&auto=format%2Ccompress&cs=srgb&q=70",
                    "Yêu xa",
                    "1000",
                    "10 phút",
                    "The diagram below shows the stages and equipment used in the cement-making process, and how \n" +
                            "cement is used to produce concrete for building purposes.",
                    "https://photographer.com.vn/wp-content/uploads/2020/08/1597480178_692_Loat-anh-Girl-Xinh-Cap-3-nu-sinh-tuoi-xi-teen.jpg",
                    100,
                    100,
                    100
                )
            )

            mListNewfeeds.add(
                Post(
                    21 + i,
                    "https://imgix.bustle.com/uploads/image/2018/3/28/17a46e1b-05c4-425a-9c98-8802a5ddac71-girlshylaughingsweetjeanjacket.jpg?w=1020&h=574&fit=crop&crop=faces&auto=format%2Ccompress&cs=srgb&q=70",
                    "Yêu xa",
                    "1000",
                    "10 phút",
                    "The diagram below shows the stages and equipment used in the cement-making process, and how \n" +
                            "cement is used to produce concrete for building purposes.",
                    "https://imgix.bustle.com/uploads/image/2018/3/28/17a46e1b-05c4-425a-9c98-8802a5ddac71-girlshylaughingsweetjeanjacket.jpg?w=1020&h=574&fit=crop&crop=faces&auto=format%2Ccompress&cs=srgb&q=70",
                    100, 100, 100
                )
            )
        }

        return mListNewfeeds
    }

}