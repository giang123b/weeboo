package com.river.weeboo.newfeeds.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.river.weeboo.R
import com.river.weeboo.newfeeds.model.ChooseImageOrVideo
import com.river.weeboo.newfeeds.model.CreateNewPost
import com.river.weeboo.newfeeds.model.Post

class NewfeedsAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_CREATE_A_POST = 1
    private val TYPE_CHOOSE_IMAGE_VIDEO = 2
    private val TYPE_POST = 3
    private var mListData = ArrayList<Any>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_CREATE_A_POST -> CreateNewPostViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.layout_create_new_post, parent, false)
            )
            TYPE_CHOOSE_IMAGE_VIDEO -> ChooseImageOrVideoViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.layout_choose_image_video_horizental, parent, false)
            )
            else -> PostViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.layout_post, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (mListData[position]) {
            is CreateNewPost -> (holder as CreateNewPostViewHolder).bindData(mListData[position] as CreateNewPost)
            is ChooseImageOrVideo -> (holder as ChooseImageOrVideoViewHolder).bindData()
            else -> (holder as PostViewHolder).bindData(mListData[position] as Post)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (mListData[position]) {
            is CreateNewPost -> TYPE_CREATE_A_POST
            is ChooseImageOrVideo -> TYPE_CHOOSE_IMAGE_VIDEO
            else -> TYPE_POST
        }
    }

    override fun getItemCount() = mListData.size

    fun submitList(data: List<Post>) {
        val diffCallback = NewFeedDiffCallback(mListData, data)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        mListData.clear()
        mListData.addAll(data)
        diffResult.dispatchUpdatesTo(this)
    }
}