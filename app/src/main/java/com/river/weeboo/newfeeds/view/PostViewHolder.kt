package com.river.weeboo.newfeeds.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.river.weeboo.R
import com.river.weeboo.newfeeds.model.Post
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_post.view.*

class PostViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    fun bindData(post: Post){

        Picasso.get().load(post.avatar).placeholder(R.drawable.avatar)
            .into(
                itemView.imageView_avatarPost_layoutPostScreen)

        itemView.textView_authorPost_layoutPostScreen.text = post.name
        itemView.textView_timePost_layoutPostScreen.text = post.time
        itemView.textView_descriptionPost_layoutPostScreen.text = post.description

        Picasso.get().load(post.image).placeholder(R.drawable.avatar)
            .into(
                itemView.imageView_imagePost_layoutPostScreen)

        itemView.textView_amountLike_layoutPostScreen.text = post.like.toString()
        itemView.textView_amountComment_layoutPostScreen.text = post.comment.toString()
        itemView.textView_amountShare_layoutPostScreen.text = post.share.toString()
    }
}