package com.river.weeboo.newfeeds.domain.remote

import com.river.weeboo.newfeeds.model.Post
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*
import kotlin.collections.ArrayList

interface ApiInterface {
    @GET("post.json")
    suspend fun getPosts():Response<ArrayList<Post>>

}