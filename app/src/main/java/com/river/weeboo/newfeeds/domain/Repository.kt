package com.river.weeboo.newfeeds.domain

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.river.weeboo.newfeeds.domain.remote.PostClient
import com.river.weeboo.newfeeds.model.Post
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class Repository : ViewModel() {
    var mutableLiveData = MutableLiveData<List<Post>>()
    fun onFailure(t: Throwable) {
    }

    fun onResponse(response: List<Post>) {
        val items: List<Post> = response
        mutableLiveData.value = items
    }

    suspend fun getPost(): MutableLiveData<List<Post>> {
        viewModelScope.launch(Dispatchers.Main) {
            val response = PostClient.getClient.getPosts()
            val posts: ArrayList<Post>? = response.body()
            mutableLiveData.value = posts

        }

        return withContext(Dispatchers.IO) {
            mutableLiveData
        }
    }

    companion object {
        private const val TAG = "Repositry"
    }

}



