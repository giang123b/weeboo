package com.river.weeboo.newfeeds.view

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.river.weeboo.newfeeds.model.Post

class NewFeedDiffCallback(private val oldList: List<Any>, private val newList: List<Any>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {

        return if (oldList[oldItemPosition] is Post && newList[newItemPosition] is Post)
            (oldList[oldItemPosition] as Post).id == (newList[newItemPosition] as Post).id
        else false
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return newList[newPosition] == oldList[oldPosition]
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}