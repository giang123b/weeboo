package com.river.weeboo.newfeeds.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.river.weeboo.R
import com.river.weeboo.newfeeds.model.CreateNewPost
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_create_new_post.view.*
import kotlinx.android.synthetic.main.layout_post.view.*
import kotlinx.android.synthetic.main.layout_post.view.imageView_imagePost_layoutPostScreen

class CreateNewPostViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    fun bindData(createNewPost: CreateNewPost){
        Picasso.get().load(createNewPost.avatar).placeholder(R.drawable.avatar)
            .into(
                itemView.imageView_avatarUser_layoutCreatePostScreen)
    }
}